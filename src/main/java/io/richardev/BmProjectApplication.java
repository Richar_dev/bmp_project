package io.richardev;

import io.richardev.services.MaestroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan({"io.richardev.dao"})
@Slf4j
public class BmProjectApplication {


  
	public static void main(String[] args) {
		SpringApplication.run(BmProjectApplication.class, args);

	}
  
}
