
package io.richardev.services;

import io.richardev.dao.MaestroDao;
import io.richardev.model.Maestro;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ricar
 */


@Service
public class MaestroServiceImpl implements MaestroService{
  
  
  @Autowired
  private MaestroDao maestroDao;

  @Override
  @Transactional(readOnly = true)
  public List<Maestro> listMaestro() {
   return (List<Maestro>) maestroDao.findAll();
  }
  
  @Override
  @Transactional(readOnly = true)
  public void list(){
    List<Maestro> ma = (List<Maestro>) maestroDao.findAll();
    
    System.out.println(ma.toString());
  }
  
  @Override
  public void saludo(String name) {
    
    System.out.println("Sigue así "+ name);
  }

  @Override
   @Transactional
  public void saveMaestro(Maestro maestro) {
    
    maestroDao.save(maestro);
    
  }

  @Override
   @Transactional
  public void deleteMaestro(Maestro maestro) {
    maestroDao.delete(maestro);
  }

  @Override
   @Transactional(readOnly = true)
  public Maestro MaestroById(Maestro maestro) {
    return maestroDao.findById(maestro.getCampo1()).orElse(null);
    
  }

  
  
}
