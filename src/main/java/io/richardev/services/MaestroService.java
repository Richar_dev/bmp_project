package io.richardev.services;

import io.richardev.model.Maestro;
import java.util.List;

/**
 *
 * @author ricar
 */


public interface MaestroService {
  
  public List<Maestro> listMaestro();
  
  public void saveMaestro(Maestro maestro);
  
  public void list();
  public void saludo(String name);
  
//  public void updateMaestro(Maestro maestro);
  
  public void deleteMaestro(Maestro maestro);
  
  public Maestro MaestroById(Maestro maestro);
  
}
