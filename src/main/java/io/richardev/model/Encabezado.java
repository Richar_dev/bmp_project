package io.richardev.model;

/**
 *
 * @author ricar
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "tbl_encabezado")
public class Encabezado implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  // ATTRIBUTES/COLUMNS
  @Id
  private String campo11;
  
  @Column(name = "campo12")
  @Temporal(TemporalType.TIMESTAMP)
  private Date campo12 = new Date();
  
  @Column(name = "campo13")
  private Integer campo13;
  
  @Column(name = "campo14")
  private String campo14;
  
  
  // CONSTRUCTORS

  public Encabezado() {
  }

  public Encabezado(String campo11) {
    this.campo11 = campo11;
  }
  
  
  // GETTERS AND SETTER
  public String getCampo11() {
    return campo11;
  }

  public void setCampo11(String campo11) {
    this.campo11 = campo11;
  }

  public Date getCampo12() {
    return campo12;
  }

  public void setCampo12(Date campo12) {
    this.campo12 = campo12;
  }

  public Integer getCampo13() {
    return campo13;
  }

  public void setCampo13(Integer campo13) {
    this.campo13 = campo13;
  }

  public String getCampo14() {
    return campo14;
  }

  public void setCampo14(String campo14) {
    this.campo14 = campo14;
  }

  @Override
  public String toString() {
    return "Encabezado{" + "campo11=" + campo11 + ", campo12=" + campo12 + ", campo13=" + campo13 + ", campo14=" + campo14 + '}';
  }
  
  
}
