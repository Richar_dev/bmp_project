
package io.richardev.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

/**
 *
 * @author ricar
 */

@Data
@Entity
@Table(name = "tbl_maestro")
public class Maestro implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  // ATTRIBUTES/COLUMNS
  @Id
  @Column(name = "campo1")
  private String campo1;
  
  @Column(name = "campo2")
  @Temporal(TemporalType.TIMESTAMP)
  private Date campo2;
  
  @Column(name = "campo3")
  private Integer campo3;
  
  @Column(name = "campo4")
  private String campo4;

  
  /// CONSTRUCTORS
  public Maestro() {
  
  }
  
  public Maestro(String campo1){
    this.campo1 = campo1;
  }
  
  
  /// GETTERS AND SETTERS
 
  public String getCampo1() {
    return campo1;
  }

  public void setCampo1(String campo1) {
    this.campo1 = campo1;
  }

  public Date getCampo2() {
    return campo2;
  }

  public void setCampo2(Date campo2) {
    this.campo2 = campo2;
  }

  public int getCampo3() {
    return campo3;
  }

  public void setCampo3(Integer campo3) {
    this.campo3 = campo3;
  }

  public String getCampo4() {
    return campo4;
  }

  public void setCampo4(String campo4) {
    this.campo4 = campo4;
  }

  @Override
  public String toString() {
    return "Maestro{" + "campo1=" + campo1 + ", campo2=" + campo2 + ", campo3=" + campo3 + ", campo4=" + campo4 + '}';
  }

  
}
