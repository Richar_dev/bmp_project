package io.richardev.model;

/**
 *
 * @author ricar
 */

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "tbl_detalle")
public class Detalle implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  // ATRIBUTES
  @Id
  @JoinColumn(name = "campo11", referencedColumnName = "campo11")
  @ManyToOne(cascade = CascadeType.ALL)
  private Encabezado campo11;
  
  @Column(name = "campo12")
  @JoinColumn(name = "campo12", referencedColumnName = "campo12")
  @ManyToOne(cascade = CascadeType.ALL)
  private Encabezado campo12;
  
  @Column(name = "campo13")
  @JoinColumn(name = "campo13", referencedColumnName = "campo13")
  @ManyToOne(cascade = CascadeType.ALL)
  private Encabezado campo13;
  
  @Column(name = "campo14")
  @JoinColumn(name = "campo14", referencedColumnName = "campo1")
  @ManyToOne(cascade = CascadeType.ALL)
  private Maestro campo14;
  
  @Column(name = "campo15")
  private String campo15;
  
  @Column(name = "campo16")
  private String campo16;
  
  
  public Detalle() {
 
  }

  public Detalle(Encabezado campo11, Encabezado campo12, Encabezado campo13, Maestro campo14, String campo15, String campo16) {
    this.campo11 = campo11;
    this.campo12 = campo12;
    this.campo13 = campo13;
    this.campo14 = campo14;
    this.campo15 = campo15;
    this.campo16 = campo16;
  }

  public Encabezado getCampo11() {
    return campo11;
  }

  public void setCampo11(Encabezado campo11) {
    this.campo11 = campo11;
  }

  public Encabezado getCampo12() {
    return campo12;
  }

  public void setCampo12(Encabezado campo12) {
    this.campo12 = campo12;
  }

  public Encabezado getCampo13() {
    return campo13;
  }

  public void setCampo13(Encabezado campo13) {
    this.campo13 = campo13;
  }

  public Maestro getCampo14() {
    return campo14;
  }

  public void setCampo14(Maestro campo14) {
    this.campo14 = campo14;
  }

  public String getCampo15() {
    return campo15;
  }

  public void setCampo15(String campo15) {
    this.campo15 = campo15;
  }

  public String getCampo16() {
    return campo16;
  }

  public void setCampo16(String campo16) {
    this.campo16 = campo16;
  }
  
  
  
}
