 
package io.richardev.dao;

import io.richardev.model.Maestro;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ricar
 */


public interface MaestroDao extends CrudRepository<Maestro, String> {
  
  
  
}
