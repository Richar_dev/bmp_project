package io.richardev.tester;

import io.richardev.model.Maestro;
import io.richardev.services.MaestroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ricar
 */

@Controller
public class TesterClass {
  
  @Autowired
  private MaestroService maestroService;
  
  
  @GetMapping("/lista")
  public List<Maestro> lista(Model model){
    
    List<Maestro> ma = maestroService.listMaestro();
    
    model.addAttribute("lista", ma);
    
    return ma;

//    
//    String help = "Ayudame";
//    
//    model.addAttribute("he", help);
//    model.addAttribute("lista", ma);
//    
//    return "index";
//    
  }
//
//  @GetMapping("/error")
//  public String err(Model model){
//    
//    String er = "Gran error 404";
//    
////    model.addAttribute("e", er);
//    model.addAttribute("e", er);
//    
//    return "error";
//  }
  
}
